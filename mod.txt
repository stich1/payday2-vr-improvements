{
	"name": "VR Enhancements",
	"description": "Bunch of tweaks for the VR Beta.",
	"author": "ZNix",
	"contact": "znix@znix.xyz",
	"version": "0.3.6",
	"image" : "vrplus.png",
	"color" : "255 255 255",
	"blt_version": 2,
	"pre_hooks" : [
		{
			"hook_id" : "lib/entry",
			"script_path" : "menus/main.lua"
		},
		{
			"hook_id" : "lib/entry",
			"script_path" : "menus/updates.lua"
		}
	],
	"hooks": [
		{
			"hook_id" : "lib/units/beings/player/states/vr/playerstandardvr",
			"script_path" : "playerstandardvr.lua"
		},
		{
			"hook_id" : "lib/units/cameras/fpcameraplayerbase",
			"script_path" : "fpcameraplayerbase.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/states/vr/playermaskoffvr",
			"script_path" : "playermaskoffvr.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/states/vr/hand/playerhandstateweapon",
			"script_path" : "playerhandstateweapon.lua"
		},
		{
			"hook_id" : "core/lib/managers/controller/corecontrollerwrappervr",
			"script_path" : "corecontrollerwrappervr.lua"
		},
		{
			"hook_id" : "lib/managers/menumanagervr",
			"script_path" : "menumanagervr.lua"
		},
		{
			"hook_id" : "lib/managers/hudmanagervr",
			"script_path" : "hud/hudmanagervr.lua"
		},
		{
			"hook_id" : "lib/managers/hud/vr/hudteammatevr",
			"script_path" : "hud/hudteammatevr.lua"
		},
		{
			"hook_id" : "lib/managers/hud/vr/hudplayerdownedvr",
			"script_path" : "hud/hudplayerdownedvr.lua"
		},
		{
			"hook_id" : "lib/input/handstatesplayer",
			"script_path" : "handstatesplayer.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/states/vr/playermenu",
			"script_path" : "playermenu.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/playerhand",
			"script_path" : "playerhand.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/playerhandstatemachine",
			"script_path" : "playerhandstatemachine.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/states/vr/hand/playerhandstateweaponassist",
			"script_path" : "playerhandstateweaponassist.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/states/vr/playertasedvr",
			"script_path" : "playertasedvr.lua"
		},
		{
			"hook_id" : "lib/units/props/ladder",
			"script_path" : "ladder.lua"
		},
		{
			"hook_id" : "lib/managers/hudmanagervr",
			"script_path" : "hudmanagervr.lua"
		}
	],
	"updates" : [
		{
			"identifier": "vrplus",
			"custom_urls": {
				"check": "https://znix.xyz/paydaymods/updates/meta.php?id=",
				"patchnotes": "https://znix.xyz/paydaymods/patchnotes/view.php?id=",
				"download": "https://znix.xyz/paydaymods/download/get.php?src=mod&id="
			}
		}
	]
}
