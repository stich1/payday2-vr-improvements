# Changelog

## V0.3.6
- Temporaraly remove camera fade options, fixing PD2VR 1.4 crash
- Fix menu laser dot colour not matching beam colour

## V0.3.5
- Fix weapon-assist toggling - Fixes #58

## V0.3.4
- Fix movement for left-handed users

## V0.3.3
- Fix jumping in PD2VRBeta update 1.3 - Fixes #57

## V0.3.2
- Update Russian translation
- Updates for VR Beta 1.3
  - Fixed crash-on-startup
  - Disable weapon-grip-toggle as it's in the base game
  - Note snap turning is not removed, as the builtin one doesn't seem to work.

## V0.3.1
- Add endscreen speedup option - Implements #40

## V0.3.0
- Fix menu options having no effect after resetting them - Fixes #50
- Fix player slowing down while quickly moving the HMD - Fixes #51

## V0.2.9
- Set default options depending on which HMD is used

## v0.2.8
- Fix fade-to-black problem - Fixes #45

## V0.2.7
- Allow player rotation while in casing mode - Fixes #44
- Fix issues with rotation jumping the player's view the first time they use it per heist.

## V0.2.6
- Add ladder support - Fixes #42

## V0.2.5
- Fix taser crash bug

## V0.2.4
- Update Russian translations (Thanks, Sergio)
- Fix weapons lagging behind their respective hand position/rotations - Fixes #38

## V0.2.3
- Add toggle weapon grip option
- Show warning when using IPHLPAPI.dll 2.0VR5 (crash-on-startup when used in VR)

## V0.2.2
- Add Korean translation (Thanks, DreadNought_40k)
- Add Spanish translation (Thanks, Souls Alive)

## V0.2.1
- Add main-menu laser pointer customization
- Update Russian translations (Thanks, Sergio)

## V0.2.0
- Add option to rebind interact control
- Add sticky-interact option

## V0.1.9.2
- Allow users to jump while in hold-to-sprint mode - Fixes #30

## V0.1.9.1
- Add Russian translations for v0.1.9.0

## V0.1.9.0
- Add HP-on-watch option (enabled by default) - Implements #16

## V0.1.8.1
- Update Russian translations for V0.1.8.0 (Thanks, Sergio)

## V0.1.8.0
- Fix player hands lagging behing camera while moving - Issue #23
- Add movement speed cap in comfort options

## V0.1.7.0:
- Add Russian translations (Thanks, Sergio)

## V0.1.6.3:
- Remove BLT hook DLL from automatic updates
- Warn user if the mod's filename is incorrect and will cause issues while updating

## V0.1.6.2:
- Fix crash on startup caused by v0.1.6.1 and extremely inadequate testing on my part
- Note this version's mod.txt says v0.1.6.1 - I forgot to update it

## V0.1.6.1:
- Add redout effect (disabled by default), fading screen to red as your health runs low - See #21

## V0.1.6:
- Add option to disable locomotion
- Warn the user if an outdated IPHLPAPI.dll is found

## V0.1.5.3:
- Add mod icon

## V0.1.5.2:
- Split camera and control options into two different menus

## V0.1.5.1:
- Fix crash when jumping while downed - See #18

## V0.1.5:
- Adds automatic updates

## V0.1.4:
- Implement controller-relative (Onward-like) movement: #8
- Fix major movement bug: #9
- Add thumbstick/trackpad-based rotation (smooth and snapping)

## V0.1.3:
- Add jumping support.
- Fix issue #4 preventing users from moving while in casing mode (not masked up).
- Adds configuration options for what the camera does when you put your head into a wall, along with defaults far better suited to locomotion movement.

## V0.1.2:
- Add deadzone slider (mainly for Vive users)

## V0.1.1:
- Add sticky sprinting checkbox (default on)

## V0.1.0:
- Initial Release

